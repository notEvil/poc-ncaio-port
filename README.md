# Proof of concept: [*Nextcloud All-in-One*](https://github.com/nextcloud/all-in-one) on custom port

As of now, using a custom port is [not officially supported](https://github.com/nextcloud/all-in-one#are-other-ports-than-the-default-443-for-nextcloud-supported). So I tried to reconfigured my instance to support it, and it works* (for now). Its a hacky mess and NOT a solution you should aim for. Rather, it should serve as starting point if you intend to experiment yourself.

**Remarks**

1. To be clear, using a custom port is not meant to elevate security.
2. You can install *Nextcloud All-in-One* [manually](https://github.com/nextcloud/all-in-one/tree/main/manual-install) and patch it to your liking. However, you loose quite a bit on the way.
3. The changes don't cover the addon Nextcloud Talk Recording-server.

*works as in, I don't see any issues/requests to incorrect URLs while trying Nextcloud, Nextcloud Office and Nextcloud Talk.

## Required changes

- Container `nextcloud-aio-nextcloud`
    - Configuration `overwrite.cli.url`
        - is set using `occ` during start of `nextcloud-aio-nextcloud`
    - Configuration `overwritehost`
        - is set by `config/reverse-proxy.config.php`
    - App settings
        - `notify_push`: `base_endpoint`
        - `richdocuments`: `wopi_url`
        - `spreed`: `signaling_servers`
            - adds a list item
        - all set using `occ` during start of `nextcloud-aio-nextcloud`
- Container `nextcloud-aio-collabora`
    - Environment variables `aliasgroup1`, `extra_params`, `server_name`
- Container `nextcloud-aio-notify-push`
    - Environment variable `NC_DOMAIN`
- Container `nextcloud-aio-talk`
    - Environment variable `NC_DOMAIN`
        - also used as turnserver realm

**Missing**

- Container `nextcloud-aio-nextcloud`
    - `run-exec-commands.sh`
- Container `nextcloud-aio-apache`
    - `healthcheck.sh`
 
## How the hack

Again, this is not something you should consider doing!

For `overwrite.cli.url` and `overwritehost` I
- set `overwritehost` using `occ`
- applied a patch to `/var/lib/docker/volumes/nextcloud_aio_nextcloud/_data/config/reverse-proxy.config.php` which removes the part related to `overwritehost`
- and use a Python script which
    - waits for `/var/lib/docker/volumes/nextcloud_aio_nextcloud/_data/config/config.php` to be written
        - then sets `overwrite.cli.url` using `occ` if necessary
    - waits for `/var/lib/docker/volumes/nextcloud_aio_nextcloud/_data/config/reverse-proxy.config.php` to be written
        - then applies the patch again

For the app settings I connected to Postgres and created a trigger for the table `oc_appconfig` which replaces the domains on insert and update. The function code:

  ```sql
  BEGIN
	NEW.configvalue := regexp_replace(NEW.configvalue, ',{"server":"https://my\.domain\.tld/standalone-signaling/","verify":true}', '', 'g');
	NEW.configvalue := regexp_replace(NEW.configvalue, 'https://my\.domain\.tld((?!:)|:443)', 'https://my.domain.tld:myport', 'g');
	RETURN NEW;
END;
```

And for the environment variables I use a Python script which
- waits for the container to start
- gets the run command using [this](https://gist.github.com/efrecon/8ce9c75d518b6eb863f667442d7bc679/raw/d469d2c60eb631fb9e226a6e67d0eb74b11ad0fd/run.tpl)
- adjusts the run command accordingly
- stops and removes the current container
- and creates and starts the container.